from __future__ import division

from YOLOv3.models import *
from YOLOv3.utils.utils import *
from YOLOv3.utils.parse_config import *

from nuscenes_datasets import NuscenesDataset
from environment import RESULT_PATH

import os
import sys
import time
import datetime
import argparse
import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import pickle

def evaluate(model, version, valtest, iou_thres, conf_thres, nms_thres, 
    img_size, batch_size, min_visibility=1, min_height=25,
    model_num=0, save_result=False):
    assert version in ['v1.0-mini','v1.0-medium', 'v1.0-trainval']
    model.eval()

    # Get dataloader
    dataset = NuscenesDataset(version, valtest, img_size, 
        min_visibility=min_visibility, min_height=min_height,
        augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=1,
        pin_memory=True,
        collate_fn=dataset.collate_fn,
    )

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)
    all_tokens = []
    all_results = []
    all_annotations = []
    for batch_i, (_, imgs, targets, tokens) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):

        # Extract labels
        labels += targets[:, 1].tolist()
        # Rescale target
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        for tkn in tokens:
            all_tokens += tkn.tolist()

        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = model(imgs)
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres)

        all_results.append(outputs)
        all_annotations.append(targets)
        sample_metrics += get_batch_statistics(outputs, targets, iou_threshold=iou_thres)

    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, labels)

    if save_result:
        stats = {
            'precision': precision,
            'recall': recall,
            'AP': AP,
            'f1': f1,
            'ap_class' : ap_class,
        }

        prediction = {
            'all_results': all_results,
            'all_annotations': all_annotations,
            'true_positives': true_positives,
            'pred_scores': pred_scores,
            'pred_labels': pred_labels,
            'all_labels': labels,
            'tokens': all_tokens
        }

        result = {
            'stats': stats,
            'prediction': prediction,
            'opt': opt
        }

        file_name = 'yolo_{}_version_{}_{}_visibility_{}_height_{}.pkl'.format(model_num, version, valtest, min_visibility, min_height)
        with open(RESULT_PATH + file_name, "wb") as f:
            pickle.dump(result, f)
        print("Saved results at " + RESULT_PATH + file_name)

    return precision, recall, AP, f1, ap_class


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", type=str, default="v1.0-mini", help="which version of nuscenes to train on")
    parser.add_argument("--min_visibility", type=int, default=1, help="minimum visibility a label should have")
    parser.add_argument("--min_height", type=int, default=25, help="minimum height of image")
    parser.add_argument("--model_num", type=int, default=1, help="which model_to_load")
    parser.add_argument("--batch_size", type=int, default=8, help="size of each image batch")
    parser.add_argument("--model_def", type=str, default="config/nuscenes.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="weights/yolov3.weights", help="path to weights file")
    parser.add_argument("--iou_thres", type=float, default=0.5, help="iou threshold required to qualify as detected")
    parser.add_argument("--conf_thres", type=float, default=0.5, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.5, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=512, help="size of each image dimension")
    parser.add_argument("-s", action='store_true', help= "save testing result")
    opt = parser.parse_args()
    print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    class_names = load_classes("config/nuscenes_classes.names")

    if opt.s and opt.model_num == -1:
        print("Specify model number!")
        sys.exit(1)

    # Initiate model
    model = Darknet(opt.model_def).to(device)
    if opt.weights_path.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(opt.weights_path)
    else:
        # Load checkpoint weights
        model.load_state_dict(torch.load(opt.weights_path))

    print("Compute mAP...")

    precision, recall, AP, f1, ap_class = evaluate(
        model,
        opt.version,
        valtest='test',
        iou_thres=opt.iou_thres,
        conf_thres=opt.conf_thres,
        nms_thres=opt.nms_thres,
        img_size=opt.img_size,
        batch_size=8,
        min_visibility=opt.min_visibility,
        min_height=opt.min_height,
        save_result=opt.s,
        model_num=opt.model_num,
    )

    print("Average Precisions:")
    for i, c in enumerate(ap_class):
        print(f"+ Class '{c}' ({class_names[c]}) - AP: {AP[i]}")

    print(f"mAP: {AP.mean()}")
