from __future__ import division

from YOLOv3.models import *
from YOLOv3.utils.utils import *

from nuscenes_datasets import NuscenesDataset

import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator
import random

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", type=str, default="v1.0-mini", help="which version of nuscenes to train on")
    parser.add_argument("--min_visibility", type=int, default=3, help="minimum visibility a label should have")
    parser.add_argument("--model_def", type=str, default="config/nuscenes-tiny.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="checkpoints/yolov3_model1_ckpt_9.pth", help="path to weights file")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.4, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--batch_size", type=int, default=1, help="size of the batches")
    parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=512, help="size of each image dimension")
    parser.add_argument("--num_imgs", type=int, default=100, help="number of images to try detecting")
    opt = parser.parse_args()
    print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    os.makedirs("output", exist_ok=True)

    # Set up model
    model = Darknet(opt.model_def, img_size=opt.img_size).to(device)

    if opt.weights_path.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(opt.weights_path)
    else:
        # Load checkpoint weights
        model.load_state_dict(torch.load(opt.weights_path))

    model.eval()  # Set in evaluation mode

    dataset = NuscenesDataset(opt.version, 'test', opt.img_size, min_visibility=opt.min_visibility, augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=opt.batch_size,
        shuffle=False,
        num_workers=opt.n_cpu,
        pin_memory=True,
        collate_fn=dataset.collate_fn,
    )

    classes = load_classes("config/nuscenes_classes.names")  # Extracts class labels from file

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    print("\nPerforming object detection:")

    model_total = datetime.timedelta(0)
    total = datetime.timedelta(0)
    total_anns = 0
    
    for batch_i, (img_paths, input_imgs, _, _) in enumerate(dataloader):
        if batch_i == opt.num_imgs:
            break

        prev_time = time.time()
        # Configure input
        input_imgs = Variable(input_imgs.type(Tensor))

        # Get detections
        with torch.no_grad():
            detections = model(input_imgs)

            model_time = time.time()
            model_time = datetime.timedelta(seconds=model_time - prev_time)
            model_total += model_time

            detections = non_max_suppression(detections, opt.conf_thres, opt.nms_thres)

        # Log progress
        inference_time = time.time()
        inference_time = datetime.timedelta(seconds=inference_time - prev_time)
        total += inference_time

        for detection in detections:
            if detection is not None:
                total_anns += detection.size(0)

        print("\t+ Batch %d, Model Time: %s" % (batch_i, model_time))
        print("\t+ Batch %d, Total Time: %s" % (batch_i, inference_time))

    print("Generated total of {} annotations (before nms)".format(total_anns))
    print("Generated avg of {} annotations per image".format(total_anns / opt.num_imgs))

    print("[Model] time: {}".format(model_total))
    print("[Model] Avg inference time per image: {}".format(model_total / opt.num_imgs))
    print("[Model] Avg frames per second: {}".format(opt.num_imgs / model_total.total_seconds()))

    print("Total inference time: {}".format(total))
    print("Avg inference time per image: {}".format(total / opt.num_imgs))
    print("Avg frames per second: {}".format(opt.num_imgs / total.total_seconds()))
