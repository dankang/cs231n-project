from __future__ import division

from YOLOv3.utils.logger import *
from YOLOv3.utils.utils import *
from YOLOv3.utils.parse_config import *


import FasterRCNN.utils as utils
from FasterRCNN.model import MyFasterRCNN, MyFasterRCNNMobile

from test_rcnn import evaluate

from nuscenes_datasets import NuscenesDatasetRCNN
from utils import save_all, timer

from terminaltables import AsciiTable

import os
import sys
import time
import datetime
import argparse

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", type=str, default="v1.0-mini", help="which version of nuscenes to train on")
    parser.add_argument("--min_visibility", type=int, default=1, help="minimum visibility a label should have")
    parser.add_argument("--epochs", type=int, default=10, help="number of epochs")
    parser.add_argument('-m', action='store_true', help="use mobile net as backbone (if false, uses resnet 50)")
    parser.add_argument('-mm', action='store_true', help="use mobile net as backbone and use mini version")
    parser.add_argument('-a', action='store_true', help="use augmentation")
    parser.add_argument("--model_num", type=int, default=1, help="number of model for record sake")
    parser.add_argument("--batch_size", type=int, default=8, help="size of each image batch")
    parser.add_argument("--gradient_accumulations", type=int, default=2, help="number of gradient accums before step")
    parser.add_argument("--pretrained_weights", type=str, help="if specified starts from checkpoint model")
    parser.add_argument("--model_def", type=str, help="unused option")
    parser.add_argument("--n_cpu", type=int, default=0, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=512, help="size of each image dimension")
    parser.add_argument("--checkpoint_interval", type=int, default=1, help="interval between saving model weights")
    parser.add_argument("--evaluation_interval", type=int, default=1, help="interval evaluations on validation set")
    parser.add_argument("--aug_prop", type=float, default=1.0, help="proportion to use for scarce data augmentation (1 = use new image as full)")
    parser.add_argument("--compute_map", default=False, help="if True computes mAP every tenth batch")
    parser.add_argument("--multiscale_training", default=True, help="allow for multi-scale training")
    opt = parser.parse_args()
    print(opt)

    print("Using conf_thres of 0.1!")
    print("Using nms_thres of 0.3!")

    eval_opts = {
        'iou_thres': 0.5,
        'conf_thres': 0.1,
        'nms_thres': 0.3,
        'batch_size': 8,
        'min_visibility': opt.min_visibility
    }

    save_all(opt, eval_opts, "FasterRCNN", opt.model_num)

    log_dir = "tensorboard/{}".format(opt.model_num)
    os.makedirs(log_dir, exist_ok=True)
    logger = Logger(log_dir)

    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter('lr', utils.SmoothedValue(window_size=1, fmt='{value:.6f}'))

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Using device {}".format(device))

    os.makedirs("output", exist_ok=True)
    os.makedirs("checkpoints", exist_ok=True)

    # Get data configuration
    class_names = load_classes("config/nuscenes_classes.names")
    print(class_names)

    # Initiate model 
    if (opt.m):
        if (opt.mm):
            model = MyFasterRCNNMobile(len(class_names)).to(device)
        else:
            model = MyFasterRCNNMobileMini(len(class_names)).to(device)
    else:
        model = MyFasterRCNN(len(class_names)).to(device)

    # If specified we start from checkpoint
    if opt.pretrained_weights:
        print("Loading pretrained weights from: {}".format(opt.pretrained_weights))
        state = torch.load(opt.pretrained_weights)
        model.load_state_dict(state['model'])

    # Make sure we are training the parameters
    for param in model.parameters():
        param.requires_grad = True

    # Get dataloader
    train_dataset = NuscenesDatasetRCNN(opt.version, 'train', opt.img_size, is_train=True, min_height=0,
        min_visibility=opt.min_visibility, augment=True, multiscale=opt.multiscale_training, 
        augment_rare_classes=opt.a, aug_prop=self.aug_prop)
    dataloader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=opt.batch_size,
        shuffle=True,
        num_workers=opt.n_cpu,
        pin_memory=True,
        collate_fn=utils.collate_fn,
    )

    optimizer = torch.optim.Adam(model.parameters(), weight_decay=0.0005)
    if opt.pretrained_weights:
        print("Loading optimizer state from: {}".format(opt.pretrained_weights))
        state = torch.load(opt.pretrained_weights)
        optimizer.load_state_dict(state['optim'])

    print_every = 1000

    # Good reference: https://github.com/pytorch/vision/blob/master/references/detection/engine.py
    for epoch in range(1, opt.epochs + 1):
        model.train()
        start_time = time.time()
        for batch_i, (_, images, targets, _) in enumerate(dataloader):
            batches_done = len(dataloader) * (epoch - 1) + batch_i

            images = list(Variable(image.to(device)) for image in images)
            targets = [{k: Variable(v.to(device), requires_grad=False) for k, v in t.items()} for t in targets]
        
            loss_dict = model(images, targets)    
            losses = sum(loss for loss in loss_dict.values())

            if not math.isfinite(losses.item()):
                print("Loss is {}, stopping training".format(losses.item()))
                print(loss_dict)
                print(targets)
                sys.exit(1)
        
            losses.backward()
            if batches_done % opt.gradient_accumulations:
                # Accumulates gradient before each step
                optimizer.step()
                optimizer.zero_grad()

            # ----------------
            #   Log progress
            # ----------------
            
            log_str = "\n---- [Epoch %d/%d, Batch %d/%d] ----\n" % (epoch, opt.epochs, batch_i, len(dataloader))
            tensorboard_log = []
            loss_str = ""
            for loss_key, loss_val in loss_dict.items():
                tensorboard_log += [(loss_key, loss_val.item())]
                loss_str += "{}: {}   ".format(loss_key, loss_val.item())
            tensorboard_log += [("loss", losses.item())]
            logger.list_of_scalars_summary(tensorboard_log, batches_done)

            log_str += "Total loss: {}  |  ".format(losses.item()) + loss_str

            # Determine approximate time left for epoch
            epoch_batches_left = len(dataloader) - (batch_i + 1)
            time_left = datetime.timedelta(seconds=epoch_batches_left * (time.time() - start_time) / (batch_i + 1))
            log_str += f"\n---- ETA {time_left}"

            if batch_i % print_every == 0:
                print(log_str)

        timer("Epoch {}/{} finished".format(epoch, opt.epochs), start_time, time.time())

        if epoch % opt.evaluation_interval == 0:
            print("\n---- Evaluating Model ----")
            
            # Evaluate the model on the validation set
            results = evaluate(
                model,
                version=opt.version,
                valtest='val',
                iou_thres=eval_opts['iou_thres'],
                conf_thres=eval_opts['conf_thres'],
                nms_thres=eval_opts['nms_thres'],
                img_size=opt.img_size,
                batch_size=eval_opts['batch_size'],
                min_visibility=eval_opts['min_visibility'],
            )

            if (results is None):
                continue

            precision, recall, AP, f1, ap_class = results
            evaluation_metrics = [
                ("val_precision", precision.mean()),
                ("val_recall", recall.mean()),
                ("val_mAP", AP.mean()),
                ("val_f1", f1.mean()),
            ]
            logger.list_of_scalars_summary(evaluation_metrics, epoch)

            # Print class APs and mAP
            ap_table = [["Index", "Class name", "AP"]]
            for i, c in enumerate(ap_class):
                ap_table += [[c, class_names[c], "%.5f" % AP[i]]]
            print(AsciiTable(ap_table).table)
            print(f"---- mAP {AP.mean()}")
            

        if epoch % opt.checkpoint_interval == 0:
            state = {
                'model': model.state_dict(),
                'optim': optimizer.state_dict() 
            }
            torch.save(state, "checkpoints/rcnn_model{}_ckpt_{}.pth".format(opt.model_num, epoch))
