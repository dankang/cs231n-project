import numpy as np
from tqdm import tqdm
import json

# Local
from constants import *
from environment import *
from utils import change_to_detection_class

'''
    First create a json file using:

    python3 export_2d_annotations_as_json.py --dataroot /lfs/local/0/dankang/nuscenes/data/sets/nuscenes/ \
        --version v1.0-mini \
        --filename annotations_2d_1234.json \
        --visibilities 1 2 3 4 

    python3 export_2d_annotations_as_json.py --dataroot /lfs/local/0/dankang/nuscenes/data/sets/nuscenes/ \
        --version v1.0-trainval \
        --filename annotations_2d_1234.json \
        --visibilities 1 2 3 4 

    Note options: 
    
    parser.add_argument('--dataroot', type=str, default='/data/sets/nuscenes', help="Path where nuScenes is saved.")
    parser.add_argument('--version', type=str, default='v1.0-trainval', help='Dataset version.')
    parser.add_argument('--filename', type=str, default='image_annotations.json', help='Output filename.')
    parser.add_argument('--visibilities', type=str, default=['1', '2', '3', '4'],
                        help='Visibility bins, the higher the number the higher the visibility.', nargs='+')
'''

def get_output_dict(anns_list):
    output_dict = {}
    print("Total of {} annotations".format(len(anns_list)))

    for entry in tqdm(anns_list):
        label = change_to_detection_class(entry['category_name'])
        if label == DetectionClass.IGNORE.value: # ignore
            continue
        
        corners = entry['bbox_corners']
        
        file_name = entry['filename'][8:-4]
        
        x_center = np.mean((corners[0], corners[2]))
        y_center = np.mean((corners[1], corners[3]))
        
        width = corners[2] - corners[0]
        height = corners[3] - corners[1]
        
        width_scaled = width / IMG_WIDTH
        height_scaled = height / IMG_HEIGHT

        x_center_scaled = x_center / IMG_WIDTH
        y_center_scaled = y_center / IMG_HEIGHT

        ann = "{} {} {} {} {} {} {}".format(label, x_center_scaled, y_center_scaled, width_scaled, height_scaled,
                                        entry['visibility_token'], entry['sample_annotation_token'])
        
        anns = output_dict.get(file_name, [])
        anns.append(ann)
        output_dict[file_name] = anns

    return output_dict

def write_output_to_files(output_dict):
    print("Total of {} images".format(len(output_dict.keys())))

    for (img_name, anns) in tqdm(output_dict.items()):
        full_path = ANNOTATION_PATH + img_name + ".txt"    
        with open(full_path, 'w') as f:
            for ann in anns:
                f.write("{}\n".format(ann))

def main():
    version = 'v1.0-trainval'
    json_name = 'annotations_2d_1234.json'

    file_name = DATASET_PATH + "/{}/{}".format(version, json_name)
    with open(file_name, 'r') as f:
        anns_list = json.load(f)

    output_dict = get_output_dict(anns_list)
    write_output_to_files(output_dict)

if __name__ == '__main__':
    main()
