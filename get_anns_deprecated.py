# Nuscenes
from nuscenes.nuscenes import NuScenes
from nuscenes.utils.geometry_utils import BoxVisibility, view_points, transform_matrix

import numpy as np
from tqdm import tqdm

# Local
from constants import *
from environment import *
from utils import change_to_detection_class, quaternion_yaw

VERSION = 'v1.0-mini' # 'v1.0-trainval'
DETECTION_LEVEL = BoxVisibility.ALL # BoxVisibility.ANY

def write_anns_to_file(boxes, camera_intrinsic, img_name):
    anns = []
    for box in boxes:
        corners = view_points(box.corners(), camera_intrinsic, normalize=True)[:2, :]
        label = change_to_detection_class(box.name)
        
        if label == 0: # ignore
            continue
            
        x_max, x_min = np.max(corners[0]), np.min(corners[0])
        y_max, y_min = np.max(corners[1]), np.min(corners[1])

        width = x_max - x_min
        height = y_max - y_min

        x_center, y_center = np.mean(corners[0]), np.mean(corners[1])
        
        width_scaled = width / IMG_WIDTH
        height_scaled = height / IMG_HEIGHT
        
        x_center_scaled = x_center / IMG_WIDTH
        y_center_scaled = y_center / IMG_HEIGHT
        
        yaw = quaternion_yaw(box.orientation)
        
        ann = "{} {} {} {} {} {}".format(label, x_center_scaled, y_center_scaled, width_scaled, height_scaled, yaw)
        anns.append(ann)

    file_name = OUTPUT_PATH + img_name[:-4] + ".txt"    
    with open(file_name, 'w') as f:
        for ann in anns:
            f.write("{}\n".format(ann))

def main():
    nusc = NuScenes(version=VERSION, dataroot='/lfs/local/0/dankang/nuscenes/data/sets/nuscenes', verbose=True)

    for sample in tqdm(nusc.sample):
        for cam in CAMERAS:
            sample_data = nusc.get('sample_data', sample['data'][cam])
            img_name = sample_data['filename'][8:]
            assert img_name.startswith(cam)
            
            (_, boxes, camera_intrinsic) = nusc.get_sample_data(sample['data'][cam], DETECTION_LEVEL)
            
            write_anns_to_file(boxes, camera_intrinsic, img_name)

if __name__ == '__main__':
    main()
