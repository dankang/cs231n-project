from nuscenes.nuscenes import NuScenes
from nuscenes.utils.splits import train as TRAIN, val as VAL, test as TEST, mini_train as MINI_TRAIN, mini_val as MINI_VAL
from tqdm import tqdm
import numpy as np
from constants import *
from environment import *
from utils import change_to_detection_class
import json
import os

#version = 'v1.0-medium'
min_height = 70
min_visibility = 4
version = 'v1.0-medium'
json_name = 'annotations_2d_1234.json' # Json file created by calling export_2d_annotations_as_json
augment = False

def write_splits(train_imgs, val_imgs, test_imgs):
    suffix = '_{}'.format(min_visibility)
    if augment:
        suffix += '_augment'
        
    with open(ANNOTATION_PATH + version + "-train" + suffix, 'w') as f:
        for img_name in train_imgs:
            f.write("{}\n".format(img_name))

    with open(ANNOTATION_PATH + version + "-val" + suffix, 'w') as f:
        for img_name in val_imgs:
            f.write("{}\n".format(img_name))

    with open(ANNOTATION_PATH + version + "-test" + suffix, 'w') as f:
        for img_name in test_imgs:
            f.write("{}\n".format(img_name))

def get_imgs(nusc, file_names, scene):
    img_set = set()
    token = scene['first_sample_token']
    while not token == "":
        sample = nusc.get('sample', token)
        for cam in CAMERAS:
            sample_data_token = sample['data'][cam]
            data = nusc.get('sample_data', sample_data_token)
            filename = data['filename'][8:-4]

            # make sure we have the annotation
            if filename not in file_names:
                continue

            # make sure the annotation is not empty
            full_path = ANNOTATION_PATH + filename + '.txt'
            if os.stat(full_path).st_size == 0:
                print('[Should not get here] The file ' + full_path + ' was empty so not adding it')
                continue

            # make sure we have more than one annotation with given min_visibility
            if min_visibility > 1:
                _, _, _, _, height, visibility, _ = np.loadtxt(full_path, dtype={\
                    'names': ('label', 'x_center', 'y_center', 'width', 'height', 'visibility', 'token'),
                    'formats': ('i','f','f','f','f','i','S40')}, unpack=True)

                # filter out by visibility and height
                visibility_filter = visibility >= min_visibility
                height_filter = 900 * height >= min_height

                if np.sum(np.logical_and(visibility_filter, height_filter)) == 0:
                    continue


            filename = "{} {}".format(filename, sample_data_token)
            img_set.add(filename)
        token = sample['next']
            
    return img_set

def get_splits(nusc, file_names, train, val, test):
    train_set = set(train)
    val_val_set = set(val)
    val_test_set = set(test)

    train_imgs, val_imgs, test_imgs = set(), set(), set()

    num_ignored = 0
    for scene in tqdm(nusc.scene):
        if scene['name'] in train_set:
            train_imgs |= get_imgs(nusc, file_names, scene)
        elif scene['name'] in val_val_set:
            val_imgs |= get_imgs(nusc, file_names, scene)
        elif scene['name'] in val_test_set:
            test_imgs |= get_imgs(nusc, file_names, scene)
        else:
            num_ignored += 1
            #print(scene['name'])
            #raise Exception("WARNING!")
    
    assert len(train_imgs | val_imgs | test_imgs) == len(train_imgs) + len(val_imgs) + len(test_imgs)

    print("Number of training imgs: {}".format(len(train_imgs)))
    print("Number of validation imgs: {}".format(len(val_imgs)))
    print("Number of testing imgs: {}".format(len(test_imgs)))
    print("Number of scenes ignored: {}".format(num_ignored))

    assert len(train_imgs | val_imgs | test_imgs) == len(train_imgs) + len(val_imgs) + len(test_imgs)

    '''
    if version != 'v1.0-medium':
        #assert len(train_imgs | val_imgs | test_imgs) == len(file_names)
    else:
        assert num_ignored == 750
    '''

    train_imgs = list(train_imgs)
    val_imgs = list(val_imgs)
    test_imgs = list(test_imgs)

    write_splits(train_imgs, val_imgs, test_imgs)

def get_file_names_in_annotations(anns_list):
    print("Extracting all file names we have from annotations")
    print("Total of {} annotations".format(len(anns_list)))

    file_names = set()
    for entry in tqdm(anns_list):
        label = change_to_detection_class(entry['category_name'])
        if label == DetectionClass.IGNORE.value: # ignore
            continue
        
        corners = entry['bbox_corners']
        
        file_name = entry['filename'][8:-4]
        file_names.add(file_name)

    print("We have total of {} imgs with at least 1 annotation".format(len(file_names)))

    return file_names

def main():
    seed = 0
    np.random.seed(seed)

    # For trainval dataset
    perm = np.random.permutation(len(VAL))
    val_val = perm[:75]
    val_test = perm[75:]
    VAL_VAL = [VAL[ind] for ind in val_val]
    VAL_TEST = [VAL[ind] for ind in val_test]

    # for mini dataset
    MINI_VAL_VAL = MINI_VAL[0]
    MINI_VAL_TEST = MINI_VAL[1]

    # for medium dataset
    np.random.seed(seed)
    perm = np.random.permutation(len(TRAIN))
    MEDIUM_TRAIN = [TRAIN[ind] for ind in perm[:70]]
    perm = np.random.permutation(len(VAL_VAL))
    MEDIUM_VAL_VAL = [VAL_VAL[ind] for ind in perm[:15]]
    MEDIUM_VAL_TEST = [VAL_TEST[ind] for ind in perm[:15]]

    if version == 'v1.0-medium':
        nusc_version = 'v1.0-trainval'
    else:
        nusc_version = version

    nusc = NuScenes(version=nusc_version, dataroot=DATASET_PATH, verbose=True)

    file_name = DATASET_PATH + "/{}/{}".format(nusc_version, json_name)
    with open(file_name, 'r') as f:
        anns_list = json.load(f)

    file_names = get_file_names_in_annotations(anns_list)

    if version == 'v1.0-mini':
        # List for val and test because they are single values 
        get_splits(nusc, file_names, MINI_TRAIN, [MINI_VAL_VAL], [MINI_VAL_TEST])
    elif version == 'v1.0-trainval':
        get_splits(nusc, file_names, TRAIN, VAL_VAL, VAL_TEST)
    elif version == 'v1.0-medium':
        get_splits(nusc, file_names, MEDIUM_TRAIN, MEDIUM_VAL_VAL, MEDIUM_VAL_TEST)

if __name__ == '__main__':
    main()