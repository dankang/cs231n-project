import glob
import random
import os
import sys
import numpy as np
from PIL import Image
import torch
import torch.nn.functional as F

from torch.utils.data import Dataset
import torchvision.transforms as transforms

import json

# local
from constants import *
from environment import *
from YOLOv3.utils.augmentations import horisontal_flip
from utils import fit_image_size, get_max_overlap

def pad_to_square(img, pad_value):
    c, h, w = img.shape
    dim_diff = np.abs(h - w)
    # (upper / left) padding and (lower / right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # Determine padding
    pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
    # Add padding
    img = F.pad(img, pad, "constant", value=pad_value)

    return img, pad


def resize(image, size):
    image = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
    return image


def random_resize(images, min_size=288, max_size=448):
    new_size = random.sample(list(range(min_size, max_size + 1, 32)), 1)[0]
    images = F.interpolate(images, size=new_size, mode="nearest")
    return images


class ImageFolder(Dataset):
    def __init__(self, folder_path, img_size=416):
        self.files = sorted(glob.glob("%s/*.*" % folder_path))
        self.img_size = img_size

    def __getitem__(self, index):
        img_path = self.files[index % len(self.files)]
        # Extract image as PyTorch tensor
        img = transforms.ToTensor()(Image.open(img_path))
        # Pad to square resolution
        img, _ = pad_to_square(img, 0)
        # Resize
        img = resize(img, self.img_size)

        return img_path, img

    def __len__(self):
        return len(self.files)


class NuscenesDataset(Dataset):
    def __init__(self, version, split, img_size, min_visibility=1, min_height=25,
        augment=True, multiscale=True, normalized_labels=True):
        assert version in ['v1.0-mini', 'v1.0-medium', 'v1.0-trainval']
        assert split in ['train', 'val', 'test']

        file_list_path = ANNOTATION_PATH + version + "-" + split + "_{}".format(min_visibility) 
        print("Reading data from : " + file_list_path)
        with open(file_list_path, 'r') as f:
            self.dataset_list = [line.rstrip('\n') for line in f]

        print("Got : {} items".format(len(self.dataset_list)))

        self.img_size = img_size
        self.augment = augment
        self.multiscale = multiscale
        self.normalized_labels = normalized_labels

        self.min_size = self.img_size - 3 * 32
        self.max_size = self.img_size + 3 * 32
        self.batch_count = 0
        self.min_visibility = min_visibility
        self.min_height = min_height

    def __getitem__(self, index):
        entry = self.dataset_list[index] # (filename token)
        root_file_name = entry.split(" ")[0] 

         # ---------
        #  Image
        # ---------
        img_path = DATASET_PATH + "samples/" + root_file_name + ".jpg"

        # Extract image as PyTorch tensor
        img = transforms.ToTensor()(Image.open(img_path).convert('RGB'))

        # Handle images with less than three channels
        if len(img.shape) != 3:
            img = img.unsqueeze(0)
            img = img.expand((3, img.shape[1:]))

        _, h, w = img.shape
        h_factor, w_factor = (h, w) if self.normalized_labels else (1, 1)
        # Pad to square resolution
        img, pad = pad_to_square(img, 0)
        _, padded_h, padded_w = img.shape

        # ---------
        #  Label
        # ---------

        label_path = ANNOTATION_PATH + root_file_name +".txt"
        targets = None
        if os.path.exists(label_path):
            label, x_center, y_center, width, height, visibility, token = np.loadtxt(label_path, dtype={\
                'names': ('label', 'x_center', 'y_center', 'width', 'height', 'visibility', 'token'),
                'formats': ('i','f','f','f','f','i','S40')}, unpack=True)
            infos = np.stack((label, x_center, y_center, width, height, visibility)).T
            token = np.atleast_1d(token)

            # filter out by visibility and height
            visibility_filter = visibility >= self.min_visibility
            height_filter = 900 * height >= self.min_height
            filter_inds = np.logical_and(visibility_filter, height_filter)
            boxes = infos[filter_inds,:-1]
            token = token[filter_inds]
            
            # Extract coordinates for unpadded + unscaled image
            x1 = w_factor * (boxes[:, 1] - boxes[:, 3] / 2)
            y1 = h_factor * (boxes[:, 2] - boxes[:, 4] / 2)
            x2 = w_factor * (boxes[:, 1] + boxes[:, 3] / 2)
            y2 = h_factor * (boxes[:, 2] + boxes[:, 4] / 2)
            # Adjust for added padding
            x1 += pad[0]
            y1 += pad[2]
            x2 += pad[1]
            y2 += pad[3]
            # Returns (x, y, w, h)
            boxes[:, 1] = ((x1 + x2) / 2) / padded_w
            boxes[:, 2] = ((y1 + y2) / 2) / padded_h
            boxes[:, 3] *= w_factor / padded_w
            boxes[:, 4] *= h_factor / padded_h

            boxes = torch.from_numpy(boxes)
            targets = torch.zeros((len(boxes), 6))
            targets[:, 1:] = boxes

        # Apply augmentations
        if self.augment:
            if np.random.random() < 0.5:
                img, targets = horisontal_flip(img, targets)

        return img_path, img, targets, token

    def collate_fn(self, batch):
        paths, imgs, targets, token = list(zip(*batch))
        # Remove empty placeholder targets
        targets = [boxes for boxes in targets if boxes is not None]
        # Add sample index to targets
        for i, boxes in enumerate(targets):
            boxes[:, 0] = i
        targets = torch.cat(targets, 0)
        # Selects new image size every tenth batch
        if self.multiscale and self.batch_count % 10 == 0:
            self.img_size = random.choice(range(self.min_size, self.max_size + 1, 32))
        # Resize images to input shape
        imgs = torch.stack([resize(img, self.img_size) for img in imgs])
        self.batch_count += 1
        return paths, imgs, targets, token

    def __len__(self):
        return len(self.dataset_list)


class NuscenesDatasetRCNN(NuscenesDataset):
    def __init__(self, version, split, img_size, is_train=True, min_visibility=1, min_height=25,
        augment=True, multiscale=True, normalized_labels=True, augment_rare_classes=False, aug_prop=1.0):
        super().__init__(version, split, img_size, min_visibility, min_height, 
            augment, multiscale, normalized_labels)
        self.is_train = is_train
        self.augment_rare_classes = augment_rare_classes
        self.aug_prop = aug_prop

        if self.augment_rare_classes:
            dict_path = AUGMENTATION_PATH + "/aug_dict.json"
            with open(dict_path, "r") as f:
                self.aug_dict = json.load(f)

            for key, value in self.aug_dict.items():
                self.aug_dict[key] = np.array(value)

    def __getitem__(self, index):
        entry = self.dataset_list[index] # (filename token)
        root_file_name = entry.split(" ")[0] 

         # ---------
        #  Image
        # ---------
        img_path = DATASET_PATH + "samples/" + root_file_name + ".jpg"

        # Extract image as PyTorch tensor
        img = Image.open(img_path).convert('RGB')
        img = transforms.Resize((450, 800))(img) # half the size
        img = transforms.ToTensor()(img)

        # Handle images with less than three channels
        if len(img.shape) != 3:
            img = img.unsqueeze(0)
            img = img.expand((3, img.shape[1:]))

        _, h, w = img.shape

        h_factor, w_factor = (h, w) if self.normalized_labels else (1, 1)

        # ---------
        #  Label
        # ---------

        label_path = ANNOTATION_PATH + root_file_name + ".txt"
        target = None
        if os.path.exists(label_path):
            label, x_center, y_center, width, height, visibility, token = np.loadtxt(label_path, dtype={\
                'names': ('label', 'x_center', 'y_center', 'width', 'height', 'visibility', 'token'),
                'formats': ('i','f','f','f','f','i','S40')}, unpack=True)
            infos = np.stack((label, x_center, y_center, width, height, visibility)).T
            token = np.atleast_1d(token)

            # filter out by visibility and height
            visibility_filter = visibility >= self.min_visibility
            height_filter = 900 * height >= self.min_height
            filter_inds = np.logical_and(visibility_filter, height_filter)

            infos = infos[filter_inds,:-1]
            token = token[filter_inds]

            # Extract coordinates for unpadded + unscaled image
            x1 = w_factor * (infos[:, 1] - infos[:, 3] / 2)
            y1 = h_factor * (infos[:, 2] - infos[:, 4] / 2)
            x2 = w_factor * (infos[:, 1] + infos[:, 3] / 2)
            y2 = h_factor * (infos[:, 2] + infos[:, 4] / 2)

            x1 = x1.reshape((-1,1))
            y1 = y1.reshape((-1,1))
            x2 = x2.reshape((-1,1))
            y2 = y2.reshape((-1,1))
            label = infos[:, 0] + 1 # 0 is the background label, but we use 0 index classes

            # Get sizes
            boxes = np.concatenate((x1, y1, x2, y2), axis=1)

            # https://github.com/jwyang/faster-rcnn.pytorch/issues/136#issuecomment-390544655
            '''
            if self.is_train:
                big_boxes = ((x2 - x1 >= 20.0) & (y2 - y1 >= 20.0)).squeeze(1)
                boxes = boxes[big_boxes,:]
                label = label[big_boxes]
            '''
            # apparently this might lead to error if it is out of bounds
            #boxes = fit_image_size(boxes, w, h)

            
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])

            if self.augment_rare_classes:
                img, label, boxes, area = self.augment_class(img, label, boxes, area)

            num_anns = boxes.shape[0]
            boxes = torch.as_tensor(boxes, dtype=torch.float32)

            image_id = torch.tensor([index])
            labels = torch.as_tensor(label, dtype=torch.int64)
            area = torch.as_tensor(area, dtype=torch.float32)

            # we use all images
            iscrowd = torch.zeros((num_anns,), dtype=torch.int64)
        else:
            print("Label path does not exist!")
            sys.exit(1)

        # Apply augmentations
        if self.augment:
            if np.random.random() < 0.5:
                img = torch.flip(img, [-1])

                left = w - boxes[:,2]
                right = w - boxes[:,0]

                boxes[:,0] = left
                boxes[:,2] = right

        target = {
                "boxes": boxes,
                "labels": labels,
                "image_id": image_id,
                "area": area,
                "iscrowd": iscrowd,
            }

        return img_path, img, target, token

    def augment_class_single(self, img, labels, boxes, areas, ind):
        ori_label = labels[ind] - 1 # as label passed in includes background

        # car -> bus, construction vehicle, motorcycle, trailer
        # pedestrian -> bicycle
        if ori_label == DetectionClass.CAR.value:
            new_label = random.choice([DetectionClass.BUS.value, DetectionClass.CONSTRUCTION_VEHICLE.value,
                DetectionClass.MOTORCYCLE.value, DetectionClass.TRAILER.value])
        elif ori_label == DetectionClass.PEDESTRIAN.value:
            new_label = DetectionClass.BICYCLE.value
        else: # no augmentation
            return img, labels, boxes, areas

        # See if there is an overlapping box between this one and others
        # as we want to avoid cases when we just remove other boxes
        # Note we cannot use IOU
        max_prop_of_self, max_prop_of_neighbor = get_max_overlap(boxes, areas, ind)
     
        # select the box of interest (x1, y1, x2, y2)
        box = boxes[ind, :].squeeze()

        # if more than 30 % of this image overlaps with other box, 
        # or if we cover a certain portion of other image, it's prob not great
        # TODO - we could play with this value
        if max_prop_of_self > 0.3 or max_prop_of_neighbor > 0.3: 
            return img, labels, boxes, areas

        # find a corresponding sample with similar area and proportion
        aug_result = self.get_augment_img(box, new_label)
        if aug_result is not None:
            aug_img, aug_w, aug_h = aug_result

            # replace the img with augmentation
            # avg_diff = (img.mean([1,2]) - aug_img.mean([1,2]))[:,None,None]
            # aug_img.add_(avg_diff)
            
            x1, y1 = int(box[0]), int(box[1])

            img[:,y1:y1 + aug_h, x1:x1 + aug_w] *= (1 - self.aug_prop)
            img[:,y1:y1 + aug_h, x1:x1 + aug_w] += aug_img * self.aug_prop

            # if replacing, just replace the label
            # if not, add the label so that it should be predicted as well
            if self.aug_prop == 1.0:
                labels[ind] = new_label + 1 # adding back 1 to take into account background
            else:
                labels = np.append(labels, new_label + 1)
                boxes = np.append(boxes, boxes[ind,:][np.newaxis,:], axis=0)
                areas = np.append(areas, areas[ind])

        return img, labels, boxes, areas


    def augment_class(self, img, labels, boxes, areas, is_xywh=False):
        # TODO - if is_xywh, change to coordinates

        # randomly select an image class we want to augment with
        # rand_ind = np.random.choice(np.arange(len(labels)))

        # go through all the labels, augment if we can
        label_len = len(labels) # as this can be modified during the run, compute this first

        for ind in range(label_len):
            img, labels, boxes, areas = self.augment_class_single(img, labels, boxes, areas, ind)
            #if np.random.random() < 0.5:
                

        return img, labels, boxes, areas

    def get_augment_img(self, box, new_label):
        aug_entries = self.aug_dict[str(new_label)]
        [x1, y1, x2, y2] = box
        width = x2 - x1
        height = y2 - y1

        ratio = width / height
        scaled_area = width * height / (800.0 * 450.0)
        
        # we want this value to be close to 1
        area_ratio = scaled_area / aug_entries[:,2] 

        # we want this value to be close to 0
        ratio_diff = np.abs(ratio - aug_entries[:,3])

        # the augmentation image can be bigger than given image, but not too much smaller
        valid_area_size = area_ratio < 1.2

        # we want the ratio different to be pretty small
        valid_ratio_diff = ratio_diff < 0.1

        valid_sub = np.logical_and(valid_area_size, valid_ratio_diff)

        if np.sum(valid_sub) == 0:
            return None

        aug_ind = np.random.choice(np.arange(aug_entries.shape[0])[valid_sub])

        img_path = AUGMENTATION_PATH + "{}/{}.jpg".format(str(new_label), aug_ind)

        width = int(width)
        height = int(height)

        if width <= 0 or height <= 0:
            return None

        img = Image.open(img_path).convert('RGB')
        img = transforms.Resize((int(height), int(width)))(img) 
        img = transforms.ToTensor()(img) 

        return img, int(width), int(height)   


    def __len__(self):
        return len(self.dataset_list)
