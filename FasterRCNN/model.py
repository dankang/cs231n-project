import torch 
import torchvision
import os
import torch.nn as nn
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.rpn import AnchorGenerator

class MyFasterRCNN(nn.Module):
    def __init__(self, num_classes):
        super(MyFasterRCNN, self).__init__()
        self.rcnn = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

        # replace the box-predictor
        in_features = self.rcnn.roi_heads.box_predictor.cls_score.in_features
        self.rcnn.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes + 1)

    def forward(self, images, targets=None):
        return self.rcnn(images, targets)


class MyFasterRCNNMobile(nn.Module):
    def __init__(self, num_classes):
        super(MyFasterRCNNMobile, self).__init__()
        # load a pre-trained model for classification and return
        # only the features
        backbone = torchvision.models.mobilenet_v2(pretrained=True).features

        # FasterRCNN needs to know the number of
        # output channels in a backbone. For mobilenet_v2, it's 1280
        # so we need to add it here
        backbone.out_channels = 1280

        # let's make the RPN generate 5 x 3 anchors per spatial
        # location, with 5 different sizes and 3 different aspect
        # ratios. We have a Tuple[Tuple[int]] because each feature
        # map could potentially have different sizes and
        # aspect ratios 
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))

        # let's define what are the feature maps that we will
        # use to perform the region of interest cropping, as well as
        # the size of the crop after rescaling.
        # if your backbone returns a Tensor, featmap_names is expected to
        # be [0]. More generally, the backbone should return an
        # OrderedDict[Tensor], and in featmap_names you can choose which
        # feature maps to use.
        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=[0],
                                                        output_size=7,
                                                        sampling_ratio=2)

        # put the pieces together inside a FasterRCNN model
        self.model = FasterRCNN(backbone,
                           num_classes=num_classes + 1,
                           rpn_anchor_generator=anchor_generator,
                           box_roi_pool=roi_pooler)

    def forward(self, images, targets=None):
        return self.model(images, targets)