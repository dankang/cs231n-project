import torch 
import torchvision
import os
import torch.nn as nn
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

class MyFasterRCNN(nn.Module):
    def __init__(self, num_classes):
        super(MyFasterRCNN, self).__init__()
        self.rcnn = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

        # replace the box-predictor
        in_features = self.rcnn.roi_heads.box_predictor.cls_score.in_features
        self.rcnn.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes + 1)

    def forward(self, images, targets=None):
        return self.rcnn(images, targets)
    
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = MyFasterRCNN(2).to(device)
    
images = list(torch.randn((1,3,512,512)).to(device))
targets = [{
        "boxes": torch.randn((1,4)),
        "labels": torch.randn((1)),
        "image_id": torch.randn((1)),
        "area": torch.randn((1)),
        "iscrowd": torch.randn((1))
    }]
targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

loss_dict = model(images, targets)