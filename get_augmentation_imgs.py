# Get augmentation imgs
from PIL import Image
from constants import DetectionClass,CAMERAS
from nuscenes.nuscenes import NuScenes
from nuscenes.utils.splits import train as TRAIN, val as VAL, test as TEST, mini_train as MINI_TRAIN, mini_val as MINI_VAL
from tqdm import tqdm
import numpy as np
from environment import *
from utils import change_to_detection_class
import json
import os

#version = 'v1.0-medium'
min_visibility_for_augmentation = 3
json_name = 'annotations_2d_1234.json' # Json file created by calling export_2d_annotations_as_json
augment = False

classes_to_augment = [cl.value for cl in [DetectionClass.BICYCLE, DetectionClass.BUS, 
    DetectionClass.MOTORCYCLE, DetectionClass.CONSTRUCTION_VEHICLE, DetectionClass.TRAILER]]

def save_imgs(labels, x_centers, y_centers, widths, heights, valid_augment, filename, aug_dict):
    labels, x_centers, y_centers, widths, heights = [ar[valid_augment] for ar in [labels, x_centers, y_centers, widths, heights]]

    filename_full = DATASET_PATH + "samples/" + filename + ".jpg"
    img = Image.open(filename_full)
    (w,h) = img.size

    for label, x_center, y_center, width, height in zip(labels, x_centers, y_centers, widths, heights):

        # Crop the image
        x_center *= w
        y_center *= h
        width *= w
        height *= h
    
        left_x = max(int(x_center - width / 2), 0)
        left_y = max(int(y_center - width / 2), 0)
        right_x = min(int(x_center + width / 2), w - 1)
        right_y = min(int(y_center + height / 2), h - 1)

        if width < 20 or height < 20:
            continue
    
        # save the info in dictionary
        label = int(label)
        entry_id = len(aug_dict[label])

        try:
            img_new = img.crop((left_x, left_y, right_x, right_y))
            img_new_name = AUGMENTATION_PATH + "{}/{}.jpg".format(label, entry_id)
            img_new.save(img_new_name)
        except:
            print(width)
            print(filename)
            print(left_x, left_y, right_x, right_y)
            sys.exit(1)

        # rescale again
        width /= w
        height /= h
        area = width * height
        ratio = width / height
        entry = [width, height, area, ratio]
        aug_dict[label].append(entry)


def save_augmentation_imgs_from_scene(nusc, file_names, scene, aug_dict):
    img_set = set()
    token = scene['first_sample_token']
    while not token == "":
        sample = nusc.get('sample', token)
        for cam in CAMERAS:
            sample_data_token = sample['data'][cam]
            data = nusc.get('sample_data', sample_data_token)
            filename = data['filename'][8:-4]

            # make sure we have the annotation
            if filename not in file_names:
                continue

            # make sure the annotation is not empty
            full_path = ANNOTATION_PATH + filename + '.txt'
            if os.stat(full_path).st_size == 0:
                print('[Should not get here] The file ' + full_path + ' was empty so not adding it')
                continue

            # look if we have annotations that we could use for augmentation
            labels, x_centers, y_centers, widths, heights, visibility, _ = np.loadtxt(full_path, dtype={\
                    'names': ('label', 'x_center', 'y_center', 'width', 'height', 'visibility', 'token'),
                    'formats': ('i','f','f','f','f','i','S40')}, unpack=True)

            augment_class = np.isin(labels, classes_to_augment)
            visible = (visibility >= min_visibility_for_augmentation)
            valid_augment = np.logical_and(augment_class, visible)

            if np.sum(valid_augment) == 0:
                continue

            augment_labels = labels[valid_augment]
            save_imgs(labels, x_centers, y_centers, widths, heights, valid_augment, filename, aug_dict)

        token = sample['next']
            
    return img_set


def save_augmentation_imgs(nusc, file_names, train):
    aug_dict = {}
    if not os.path.exists(AUGMENTATION_PATH):
        os.makedirs(AUGMENTATION_PATH)

    for class_val in classes_to_augment:
        aug_dict[class_val] = []
        if not os.path.exists(AUGMENTATION_PATH + str(class_val)):
            os.makedirs(AUGMENTATION_PATH + str(class_val))

    train_set = set(train)

    for scene in tqdm(nusc.scene):
        if scene['name'] in train_set:
            save_augmentation_imgs_from_scene(nusc, file_names, scene, aug_dict)

    with open(AUGMENTATION_PATH + "aug_dict.json", "w") as f:
        json.dump(aug_dict, f)


def get_file_names_in_annotations(anns_list):
    print("Extracting file names we have from annotations that have classes we want to augment")
    print("Total of {} annotations".format(len(anns_list)))

    file_names = set()
    for entry in tqdm(anns_list):
        label = change_to_detection_class(entry['category_name'])
        if label not in classes_to_augment:
            continue
        
        corners = entry['bbox_corners']
        
        file_name = entry['filename'][8:-4]
        file_names.add(file_name)

    print("We have total of {} imgs with at least 1 augment class annotation".format(len(file_names)))

    return file_names

def main():
    seed = 0
    np.random.seed(seed)

    # TODO change here and down below
    #nusc_version = 'v1.0-mini'
    nusc_version = 'v1.0-trainval'

    # for medium dataset
    np.random.seed(seed)
    perm = np.random.permutation(len(TRAIN))
    MEDIUM_TRAIN = [TRAIN[ind] for ind in perm[:70]]

    nusc = NuScenes(version=nusc_version, dataroot=DATASET_PATH, verbose=True)

    file_name = DATASET_PATH + "/{}/{}".format(nusc_version, json_name)
    with open(file_name, 'r') as f:
        anns_list = json.load(f)

    file_names = get_file_names_in_annotations(anns_list)

    #save_augmentation_imgs(nusc, file_names, MINI_TRAIN)
    save_augmentation_imgs(nusc, file_names, MEDIUM_TRAIN)

if __name__ == '__main__':
    main()
