from __future__ import division

from YOLOv3.utils.utils import *

import FasterRCNN.utils as utils
from nuscenes_datasets import NuscenesDatasetRCNN
from torchvision.ops.boxes import batched_nms 
from FasterRCNN.model import MyFasterRCNN, MyFasterRCNNMobile
from environment import RESULT_PATH

import os
import sys
import time
import datetime
import argparse
import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import pickle

def evaluate(model, version, valtest, iou_thres, conf_thres, nms_thres, 
    img_size, batch_size, min_visibility=1, min_height=25,
    save_result=False, model_num=0):
    assert version in ['v1.0-mini','v1.0-medium', 'v1.0-trainval']
    model.eval()

    # Get dataloader
    dataset = NuscenesDatasetRCNN(version, valtest, img_size, 
        min_visibility=min_visibility, min_height=min_height,
        is_train=False, augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=1,
        pin_memory=True,
        collate_fn=utils.collate_fn,
    )

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    cpuTensor = torch.FloatTensor
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    cpu_device = torch.device("cpu")

    all_labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)
    all_tokens = []
    all_results = []
    all_annotations = []
    for batch_i, (_, images, targets, tokens) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):

        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        # get output
        with torch.no_grad():
            outputs = model(images)

        # TODO - see if we need this change
        outputs = [{k: v.to(cpu_device) for k, v in t.items()} for t in outputs]

        results = []
        annotations = []
        for tkn in tokens:
            all_tokens += tkn.tolist()

        for ind, (output, target) in enumerate(zip(outputs, targets)):
            # save the annotations in particular format
            num_ann = target["boxes"].shape[0]
            image_inds = (torch.ones((num_ann, 1)) * ind).type(cpuTensor)
            labels = target["labels"] - 1.0 # change back to actual classes (without background)

            ann = torch.cat((image_inds, labels.view((-1,1)).type(cpuTensor), target["boxes"].type(cpuTensor)), 1)
            annotations.append(ann)
            
            # change back to actual classes (without background)
            all_labels += (labels).tolist()

            # TODO - check when we have zero 
            pred_boxes = output["boxes"]
            pred_scores = output["scores"]
            pred_labels = output["labels"]
 
            if pred_boxes.shape[0] == 0:
                results.append(None)
                continue

            # filter results by confidence threshold
            keep_conf = pred_scores >= conf_thres
            pred_boxes = pred_boxes[keep_conf,:]
            pred_scores = pred_scores[keep_conf]
            pred_labels = pred_labels[keep_conf]

            # Perform non maximum supression
            keep_nms = batched_nms(pred_boxes, pred_scores, pred_labels, nms_thres)

            pred_boxes = pred_boxes[keep_nms,:]
            pred_scores = pred_scores[keep_nms]
            pred_labels = pred_labels[keep_nms]
            
            # filter out background prediction
            keep_labels = pred_labels != 0
            pred_boxes = pred_boxes[keep_labels,:].type(cpuTensor)
            pred_scores = pred_scores[keep_labels].type(cpuTensor)
            pred_labels = pred_labels[keep_labels].type(cpuTensor) 
            pred_labels = pred_labels - 1.0 # change to original class
            if pred_boxes.shape[0] == 0:
                results.append(None)
                continue

            # Formulate output in order as used in get_batch_statistics
            preds = torch.cat((pred_boxes, pred_scores.view((-1,1)), pred_labels.view((-1,1))),1)
            results.append(preds)

        annotations = torch.cat(annotations, 0)

        all_results.append(results)
        all_annotations.append(annotations)
        sample_metrics += get_batch_statistics(results, annotations, iou_threshold=iou_thres)

    if (len(sample_metrics)) == 0:
        print("We did not get any detection results that were good enough!... :'(")
        return None
    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, all_labels)

    if save_result:
        stats = {
            'precision': precision,
            'recall': recall,
            'AP': AP,
            'f1': f1,
            'ap_class' : ap_class,
        }

        prediction = {
            'all_results': all_results,
            'all_annotations': all_annotations,
            'true_positives': true_positives,
            'pred_scores': pred_scores,
            'pred_labels': pred_labels,
            'all_labels': all_labels,
            'tokens': all_tokens
        }

        result = {
            'stats': stats,
            'prediction': prediction,
            'opt': opt
        }

        file_name = 'rcnn_{}_version_{}_{}_visibility_{}_height_{}.pkl'.format(model_num, version, valtest, min_visibility, min_height)
        with open(RESULT_PATH + file_name, "wb") as f:
            pickle.dump(result, f)
        print("Saved results at " + RESULT_PATH + file_name)
        

    return precision, recall, AP, f1, ap_class


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", type=str, default="v1.0-mini", help="which version of nuscenes to train on")
    parser.add_argument('-m', action='store_true', help="use mobile net as backbone (if false, uses resnet 50)")
    parser.add_argument("--min_visibility", type=int, default=1, help="minimum visibility a label should have")
    parser.add_argument("--min_height", type=int, default=25, help="minimum height of image")
    parser.add_argument("--model_num", type=int, default=-1, help="which model_to_load")
    parser.add_argument("--batch_size", type=int, default=8, help="size of each image batch")
    parser.add_argument("--weights_path", type=str, default="", help="path to weights file")
    parser.add_argument("--iou_thres", type=float, default=0.5, help="iou threshold required to qualify as detected")
    parser.add_argument("--conf_thres", type=float, default=0.5, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.5, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=512, help="size of each image dimension")
    parser.add_argument("-s", action='store_true', help= "save testing result")
    opt = parser.parse_args()
    print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    class_names = load_classes("config/nuscenes_classes.names")

    # Initiate model 
    if (opt.m):
        model = MyFasterRCNNMobile(len(class_names)).to(device)
    else:
        model = MyFasterRCNN(len(class_names)).to(device)

    if opt.s and opt.model_num == -1:
        print("Specify model number!")
        sys.exit(1)

    # If specified we start from checkpoint
    if opt.weights_path == "":
        print("WARNING: there is no weight specified. We will be using untrained network.")
    else:
        print("Loading pretrained weights from: {}".format(opt.weights_path))
        state = torch.load(opt.weights_path)
        model.load_state_dict(state['model'])
        

    print("Compute mAP...")
    #print("We are evaluating on validation set!")

    precision, recall, AP, f1, ap_class = evaluate(
        model,
        opt.version,
        valtest='test',
        iou_thres=opt.iou_thres,
        conf_thres=opt.conf_thres,
        nms_thres=opt.nms_thres,
        img_size=opt.img_size,
        batch_size=8,
        min_visibility=opt.min_visibility,
        min_height=opt.min_height,
        save_result=opt.s,
        model_num=opt.model_num
    )

    print("Average Precisions:")
    for i, c in enumerate(ap_class):
        print(f"+ Class '{c}' ({class_names[c]}) - AP: {AP[i]}")

    print(f"mAP: {AP.mean()}")
