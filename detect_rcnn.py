from __future__ import division

from YOLOv3.utils.utils import *

from nuscenes_datasets import NuscenesDatasetRCNN
import FasterRCNN.utils as utils
from torchvision.ops.boxes import batched_nms 
from FasterRCNN.model import MyFasterRCNN, MyFasterRCNNMobile


import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator
import random

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", type=str, default="v1.0-mini", help="which version of nuscenes to train on")
    parser.add_argument("--weights_path", type=str, default="checkpoints/yolov3_model1_ckpt_9.pth", help="path to weights file")
    parser.add_argument('-m', action='store_true', help="use mobile net as backbone (if false, uses resnet 50)")
    parser.add_argument("--min_visibility", type=int, default=3, help="minimum visibility a label should have")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.4, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--batch_size", type=int, default=1, help="size of the batches")
    parser.add_argument("--n_cpu", type=int, default=0, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=512, help="size of each image dimension")
    parser.add_argument("--num_imgs", type=int, default=10, help="number of images to try detecting")
    opt = parser.parse_args()
    print(opt)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    os.makedirs("output", exist_ok=True)
    classes = load_classes("config/nuscenes_classes.names")  # Extracts class labels from file

    # Initiate model 
    if (opt.m):
        model = MyFasterRCNNMobile(len(classes)).to(device)
    else:
        model = MyFasterRCNN(len(classes)).to(device)

    print("TODO remember to load weights")
    
    print("Loading pretrained weights from: {}".format(opt.weights_path))
    state = torch.load(opt.weights_path)
    model.load_state_dict(state['model'])
   

    model.eval()  # Set in evaluation mode

    dataset = NuscenesDatasetRCNN(opt.version, 'test', opt.img_size, 
        is_train=False, min_visibility=opt.min_visibility, augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=opt.batch_size,
        shuffle=True,
        num_workers=opt.n_cpu,
        pin_memory=True,
        collate_fn=utils.collate_fn,
    )

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    cpuTensor = torch.FloatTensor

    imgs = []  # Stores image paths
    img_detections = []  # Stores detections for each image index

    print("\nPerforming object detection:")
    prev_time = time.time()
    for batch_i, (img_paths, input_imgs, _) in enumerate(dataloader):
        if batch_i == opt.num_imgs:
            break

        # Configure input
        input_imgs = list(image.to(device) for image in input_imgs)

        # Get detections
        with torch.no_grad():
            detections = model(input_imgs)

        results = []
        annotations = []
        for detection in detections:
            pred_boxes = detection["boxes"]
            pred_scores = detection["scores"]
            pred_labels = detection["labels"]

            if pred_boxes.shape[0] == 0:
                continue

            # filter results by confidence threshold
            keep_conf = pred_scores >= opt.conf_thres
            pred_boxes = pred_boxes[keep_conf,:]
            pred_scores = pred_scores[keep_conf]
            pred_labels = pred_labels[keep_conf]

            # Perform non maximum supression
            keep_nms = batched_nms(pred_boxes, pred_scores, pred_labels, opt.nms_thres)

            if keep_nms.shape[0] == 0:
                results.append(None)
                continue

            pred_boxes = pred_boxes[keep_nms,:]
            pred_scores = pred_scores[keep_nms]
            pred_labels = pred_labels[keep_nms]

            # filter out background prediction
            keep_labels = pred_labels != 0
            pred_boxes = pred_boxes[keep_labels,:].type(cpuTensor)
            pred_scores = pred_scores[keep_labels].type(cpuTensor)
            pred_labels = pred_labels[keep_labels].type(cpuTensor)
            pred_labels = pred_labels - 1.0

            if pred_boxes.shape[0] == 0:
                results.append(None)
                continue

            # Formulate output in order as used in get_batch_statistics
            preds = torch.cat((pred_boxes, pred_scores.view((-1,1)), pred_labels.view((-1,1))),1)
            results.append(preds)

        # Log progress
        current_time = time.time()
        inference_time = datetime.timedelta(seconds=current_time - prev_time)
        prev_time = current_time
        print("\t+ Batch %d, Inference Time: %s" % (batch_i, inference_time))

        # Save image and detections
        imgs.extend(img_paths)
        img_detections.extend(results)

    # Bounding-box colors
    cmap = plt.get_cmap("tab20b")
    colors = [cmap(i) for i in np.linspace(0, 1, 20)]

    print("\nSaving images:")
    # Iterate through images and save plot of detections
    for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):

        print("(%d) Image: '%s'" % (img_i, path))

        # Create plot
        img = np.array(Image.open(path))
        plt.figure()
        fig, ax = plt.subplots(1)
        ax.imshow(img)

        # Draw bounding boxes and labels of detections
        if detections is not None:
            # Rescale boxes to original image
            # TODO - check if this right
            print("width: {}".format(img.shape[1]))
            print("height: {}".format(img.shape[0]))
            w_factor = img.shape[1] / 800.0 # This is defined in our dataset
            h_factor = img.shape[0] / 450.0 # This is defined in our dataset
            detections[:, 0] *= w_factor
            detections[:, 2] *= w_factor
            detections[:, 1] *= h_factor
            detections[:, 3] *= h_factor

            for x1, y1, x2, y2, conf, cls_pred in detections:

                print("\t+ Label: %s, Conf: %.5f" % (classes[int(cls_pred)], conf.item()))

                box_w = x2 - x1
                box_h = y2 - y1

                color = colors[int(cls_pred.item())]
                # Create a Rectangle patch
                bbox = patches.Rectangle((x1, y1), box_w, box_h, linewidth=2, edgecolor=color, facecolor="none")
                # Add the bbox to the plot
                ax.add_patch(bbox)
                # Add label
                plt.text(
                    x1,
                    y1,
                    s=classes[int(cls_pred)],
                    color="white",
                    verticalalignment="top",
                    bbox={"color": color, "pad": 0},
                )

        # Save generated image with detections
        plt.axis("off")
        plt.gca().xaxis.set_major_locator(NullLocator())
        plt.gca().yaxis.set_major_locator(NullLocator())
        filename = path.split("/")[-1].split(".")[0]
        plt.savefig(f"output/{filename}.png", bbox_inches="tight", pad_inches=0.0)
        plt.close()
