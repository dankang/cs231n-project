from constants import YOLOClass, DetectionClass
import numpy as np
#from pyquaternion import Quaternion

'''
    Labels
'''
# Map from yolo class labels to enum value
def to_yolo_class(val):
    if val == 0:
        return YOLOClass.PERSON.value
    elif val == 1:
        return YOLOClass.BICYCLE.value
    elif val == 2:
        return YOLOClass.CAR.value
    elif val == 3:
        return YOLOClass.MOTORBIKE.value
    elif val == 5:
        return YOLOClass.BUS.value
    elif val == 7:
        return YOLOClass.TRUCK.value
    else:
        return YOLOClass.IGNORE.value

def yolo_class_to_detection_class(yolo_class):
    if (yolo_class == YOLOClass.IGNORE.value):
        detection_class = DetectionClass.IGNORE
    elif (yolo_class == YOLOClass.PERSON.value):
        detection_class = DetectionClass.PEDESTRIAN
    elif (yolo_class == YOLOClass.BICYCLE.value):
        detection_class = DetectionClass.BICYCLE
    elif (yolo_class == YOLOClass.CAR.value):
        detection_class = DetectionClass.CAR
    elif (yolo_class == YOLOClass.MOTORBIKE.value):
        detection_class = DetectionClass.MOTORCYCLE
    elif (yolo_class == YOLOClass.BUS.value):
        detection_class = DetectionClass.BUS
    elif (yolo_class == YOLOClass.TRUCK.value):
        detection_class = DetectionClass.TRUCK
    else:
        raise Exception('Invalid yolo class ' + yolo_class)

    return detection_class.value

v_to_yolo = np.vectorize(to_yolo_class)
v_yolo_to_detection = np.vectorize(yolo_class_to_detection_class)

def filter_valid_detections(detections, Tensor):
    labels = detections[:,-1]
    yolo_labels = v_to_yolo(labels)
    detections[:,-1] = torch.from_numpy(yolo_labels).type(Tensor)
    mask = detections[:,-1] > 0 # valid labels
    detections = detections[mask,:]

    return detections

def detection_class_to_yolo_class(detection_class):
    if (detection_class == DetectionClass.IGNORE.value):
        yolo_class = YOLOClass.IGNORE
    elif (detection_class == DetectionClass.BICYCLE.value):
        yolo_class = YOLOClass.BICYCLE
    elif (detection_class == DetectionClass.BUS.value):
        yolo_class = YOLOClass.BUS
    elif (detection_class == DetectionClass.CAR.value):
        yolo_class = YOLOClass.CAR
    elif (detection_class == DetectionClass.MOTORCYCLE.value):
        yolo_class = YOLOClass.MOTORBIKE
    elif (detection_class == DetectionClass.PEDESTRIAN.value):
        yolo_class = YOLOClass.PERSON
    elif (detection_class == DetectionClass.TRUCK.value):
        yolo_class = YOLOClass.TRUCK
    else:
        yolo_class = YOLOClass.NONE

    return yolo_class.value


# change from general_class to detection class as defined in https://www.nuscenes.org/object-detection
def change_to_detection_class(general_class:str, get_string=False):
    detection_class = None
    if general_class in ['animal','human.pedestrian.personal_mobility','human.pedestrian.stroller',
                         'human.pedestrian.wheelchair','movable_object.debris','movable_object.pushable_pullable',
                         'static_object.bicycle_rack','vehicle.emergency.ambulance','vehicle.emergency.police']:
        detection_class = None if get_string else DetectionClass.IGNORE
    elif general_class in ['movable_object.barrier']:
        detection_class = 'barrier' if get_string else DetectionClass.BARRIER
    elif general_class in ['vehicle.bicycle']:
        detection_class = 'bicycle' if get_string else DetectionClass.BICYCLE
    elif general_class in ['vehicle.bus.bendy', 'vehicle.bus.rigid']:
        detection_class = 'bus' if get_string else DetectionClass.BUS
    elif general_class in ['vehicle.car']:
        detection_class = 'car' if get_string else DetectionClass.CAR
    elif general_class in ['vehicle.construction']:
        detection_class = 'construction_vehicle' if get_string else DetectionClass.CONSTRUCTION_VEHICLE
    elif general_class in ['vehicle.motorcycle']:
        detection_class = 'motorcycle' if get_string else DetectionClass.MOTORCYCLE
    elif general_class in ['human.pedestrian.adult','human.pedestrian.child','human.pedestrian.construction_worker','human.pedestrian.police_officer']:
        detection_class = 'pedestrian' if get_string else DetectionClass.PEDESTRIAN
    elif general_class in ['movable_object.trafficcone']:
        detection_class = 'traffic_cone' if get_string else DetectionClass.TRAFFIC_CONE
    elif general_class in ['vehicle.trailer']:
        detection_class = 'trailer' if get_string else DetectionClass.TRAILER
    elif general_class in ['vehicle.truck']:
        detection_class = 'truck' if get_string else DetectionClass.TRUCK
    else:
        raise Exception('Invalid general class ' + general_class)

    if get_string:
        return detection_class
    else:
        return detection_class.value


'''
    Data
'''

def train_val_split(seed, len_data, train_proportion=0.9):
    assert 0 < train_proportion < 1
    
    np.random.seed(seed)
    perm = np.random.permutation(len_data)
    
    train_end = int(train_proportion * len_data)
    train = perm[:train_end]
    val = perm[train_end:]
    
    return train, val


# source: https://github.com/eriklindernoren/PyTorch-YOLOv3
def horizontal_flip(images, targets):
    images = torch.flip(images, [-1])
    targets[:, 2] = 1 - targets[:, 2]
    return images, targets

'''
    Math
'''

'''
def quaternion_yaw(q: Quaternion, in_image_frame: bool=True) -> float:
    if in_image_frame:
        v = np.dot(q.rotation_matrix, np.array([1, 0, 0]))
        yaw = -np.arctan2(v[2], v[0])
    else:
        v = np.dot(q.rotation_matrix, np.array([1, 0, 0]))
        yaw = np.arctan2(v[1], v[0])

    return yaw
'''

'''
    Training
'''

def save_all(opt, eval_opt, model_type, model_num):
    log_path = "logs/{}_{}.dump".format(model_type, model_num)
    with open(log_path, "w") as f:
        # save options
        f.write("WRITING OPTIONS\n\n")
        for k,v in sorted(vars(opt).items()): 
            f.write("{}: {}\n".format(k, v))

        f.write("\n\nWRITING EVALUATION OPTIONS\n\n")
        for k,v in sorted(eval_opt.items()): 
            f.write("{}: {}\n".format(k, v))

        if opt.model_def:
            with open(opt.model_def, "r") as d:
                model_def = d.readlines()

            for line in model_def:
                f.write(line)

    print("Saved all options at {}".format(log_path))

def timer(task, start, end):
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    time_text = "\n------[{}]{:0>2}:{:0>2}:{:05.2f}------\n".format(task, int(hours),int(minutes),seconds)
    print(time_text)
            

def fit_image_size(boxes, w, h):
    x1_too_small = (boxes[:,0] <= 0)
    boxes[x1_too_small,0] = 0.1

    y1_too_small = (boxes[:,1] <= 0)
    boxes[y1_too_small,1] = 0.1

    x2_too_big = (boxes[:,2] >= w)
    boxes[x2_too_big,2] = w - 0.1

    y2_too_big = (boxes[:,3] >= h)
    boxes[y2_too_big,3] = h - 0.1

    return boxes

def get_max_overlap(boxes, areas, rand_ind):
    '''
        Here boxes is 2d numpy array of all boxes detected
    '''
    if boxes.shape[0] == 1:
        return 0, 0

    box = boxes[rand_ind,:]
    boxes = np.delete(boxes, rand_ind, axis=0)
    area = areas[rand_ind]
    areas = np.delete(areas, rand_ind, axis=0)

    b_x1, b_y1, b_x2, b_y2 = box[0], box[1], box[2], box[3]
    bs_x1, bs_y1, bs_x2, bs_y2 = boxes[:,0], boxes[:,1], boxes[:,2], boxes[:,3]

    # get coordinates of intersection rectangle
    inter_rect_x1 = np.maximum(b_x1, bs_x1)
    inter_rect_y1 = np.maximum(b_y1, bs_y1)
    inter_rect_x2 = np.minimum(b_x2, bs_x2)
    inter_rect_y2 = np.minimum(b_y2, bs_y2)

    # get intersection area
    width = np.maximum(0, inter_rect_x2 - inter_rect_x1)
    height = np.maximum(0, inter_rect_y2 - inter_rect_y1)
    inter_areas = width * height

    # Hopefully there is no size 0 box
    max_area = np.max(inter_areas)
    max_prop_of_self = max_area / area
    max_prop_of_neighbor = np.max(inter_areas / areas)

    return max_prop_of_self, max_prop_of_neighbor

